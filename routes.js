const app = require('./app');
const {validateId} = require('./validator')

const UserController = require('./controllers/user');

app.get('/users', UserController.all);
app.post('/users', [validateId], UserController.create);