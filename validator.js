const {check} = require('express-validator')

module.exports = {
    validateId: check('id')
        .exists()
        .isInt()
        .withMessage('Id not validate')
}