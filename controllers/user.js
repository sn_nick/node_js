const client = require('../redis');
const {validationResult} = require('express-validator')
const seconds = 300;

exports.all = function (req, res) {
    client.keys('user_ids:*', function (e, keys) {
        if (e) {
            return res.status(500).send({
                status: 'error',
                message: 'Error keys'
            });
        }
        if (!keys.length) {
            return res.send({
                data: {
                    'user_ids': [],
                    'user_count': 0
                },
                status: 'ok'
            });
        }
        client.mget(keys, function (err, data) {
            if (err) {
                return res.status(500).send({
                    status: 'error',
                    message: 'Error data'
                });
            }
            res.send({
                data: {
                    'user_ids': data.map(item => parseInt(item, 10)),
                    'user_count': data.length
                },
                status: 'ok'
            });
        });
    });
}

exports.create = function (req, res) {
    const errors = validationResult(req)

    if (!errors.isEmpty()) {
        return res.status(422).send({
            status: 'error',
            message: 'Error validation id'
        })
    }

    const id = req.body.id;

    client.setex('user_ids:' + id, seconds, id, function (err) {
        if (err) {
            return res.status(500).send({
                status: 'error',
                message: 'Error add id'
            });
        }

        return res.status(201).send({
            status: 'ok'
        });
    });
}